package db

class AccountDoNotExistException(val message:String) extends Exception(message)
