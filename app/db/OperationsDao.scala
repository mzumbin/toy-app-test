package db

import javax.inject.Singleton

import com.google.inject.ImplementedBy
import model.Operation

import scala.collection.mutable
@ImplementedBy(classOf[OperationsDaoMemory])
trait OperationsDao {
   def getOperationsByAccount(account: String):List[Operation]

   def insert(operation: Operation):Unit

    def clear(): Unit
}



/**
  * fake db blocking :(
  * could use concurrent map (scala standard libs or scala stm) to control the inserts,  get all the results and  then filtered with the account number
  *
  */
@Singleton
class OperationsDaoMemory ()extends OperationsDao{


   import java.util.concurrent.locks.ReentrantReadWriteLock

   private val m =  scala.collection.mutable.Map.empty[String,List[Operation]]
   private val rwl = new ReentrantReadWriteLock
   private val r =rwl.readLock()
   private val w =rwl.writeLock()
   override def getOperationsByAccount(account: String): List[Operation] = {


      r.lock
      try {
         return m.get(account).getOrElse(throw new AccountDoNotExistException(account))

      }
      finally r.unlock
   }

   override def insert(operation: Operation): Unit = {

      w.lock()
      try {
         m(operation.accountNumber) = operation::m.get(operation.accountNumber).getOrElse(Nil)
      }
      finally w.unlock()

   }

   override def clear(): Unit = {
     w.lock()
     try {
       m.clear()
     }
     finally w.unlock()

   }
}


