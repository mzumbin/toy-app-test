import java.time.temporal.ChronoUnit
import java.time.{Instant, LocalDate, ZonedDateTime}

import model.Operation
import model.Operation.{Purchase, ValueBigDecimal, Withdrawal}

Instant.now().toString
Instant.parse( "2017-11-24T01:43:41.606Z")

import java.time.ZoneId

val zone =ZoneId.of("America/Sao_Paulo")
ZonedDateTime.now(zone)
LocalDate.MIN
ZonedDateTime.ofInstant(Instant.parse( "2017-11-24T01:43:41.606Z"),zone)

import java.time.LocalDateTime
import java.time.ZoneId
val ldt = LocalDateTime.ofInstant(Instant.parse( "2017-11-24T01:43:41.606Z"), ZoneId.of("America/Sao_Paulo"))
val day= ldt.getDayOfYear
val year= ldt.getYear
Seq(LocalDate.of(ldt.getYear+1,ldt.getMonth,ldt.getDayOfMonth),LocalDate.of(ldt.getYear,ldt.getMonth,ldt.getDayOfMonth),LocalDate.of(ldt.getYear,ldt.getMonth,ldt.getDayOfMonth)).groupBy(p=>p)
List(1,2,3,4,5).sliding(2,1).toArray

Instant.now().minus(13,ChronoUnit.DAYS).toString

