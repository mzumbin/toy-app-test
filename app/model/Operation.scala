package model

import java.time.{Instant, LocalDate, LocalDateTime, ZoneId}

import play.api.libs.json.{JsValue, Json}

sealed trait Operation {
  def value: String

  def accountNumber: String

  def date: String

  def description: Option[String]
}

object Operation {

  implicit def toBigDecimal(op: Operation): BigDecimal = {
    ValueBigDecimal(op)
  }


  def isPutting(o: Operation): Boolean = {
    o match {
      case _: Deposit => true
      case _: Salaries => true
      case _: Credits => true
      case _ => false
    }
  }

  def isTaking(op: Operation) = !isPutting(op)

  def stringInstantToLocaldate(instant: String): LocalDate = {
    insantToLocalDate(Instant.parse(instant))
  }

  def insantToLocalDate(instant: Instant): LocalDate = {
    val localDateTime = LocalDateTime.ofInstant(instant, ZoneId.of("America/Sao_Paulo"))
    LocalDate.of(localDateTime.getYear, localDateTime.getMonth, localDateTime.getDayOfMonth)
  }

  object ValueBigDecimal {
    def unapply(op: Operation): Option[BigDecimal] = {
      if (isPutting(op))
        Some(BigDecimal(op.value))
      else
        Some(-BigDecimal(op.value))
    }

    def apply(op: Operation): BigDecimal = if (isPutting(op)) BigDecimal(op.value) else -BigDecimal(op.value)

  }


  case class Deposit(value: String, accountNumber: String, date: String, description: Option[String] = None) extends Operation

  case class Salaries(value: String, accountNumber: String, date: String, description: Option[String] = None) extends Operation

  case class Credits(value: String, accountNumber: String, date: String, description: Option[String] = None) extends Operation

  case class Purchase(value: String, accountNumber: String, date: String, description: Option[String] = None) extends Operation

  case class Withdrawal(value: String, accountNumber: String, date: String, description: Option[String] = None) extends Operation

  case class Debit(value: String, accountNumber: String, date: String, description: Option[String] = None) extends Operation


  def getypeString(op: Operation) = {
    op match {
      case _: Deposit => "deposit"
      case _: Salaries => "salaries"
      case _: Credits => "credits"
      case _: Purchase => "purchase"
      case _: Withdrawal => "withdrawal"
      case _: Debit => "debit"
    }
  }


  def operationToJson(op: Operation) = {
    Json.obj(
      "value" -> op.value,
      "account_number" -> op.accountNumber,
      "date" -> op.date,
      "description" -> op.description,
      "type" -> getypeString(op)

    )
  }

  def readOperationJson(op: JsValue): Operation = {
    val value = BigDecimal((op \ "value").as[String]).abs.toString()
    val accountNumber = (op \ "account_number").as[String]
    val date = (op \ "date").as[String]
    val description = (op \ "description").asOpt[String]
    val typeString = (op \ "type").as[String]

    typeString match {
      case "deposit" => Deposit(value, accountNumber, date, description)
      case "salaries" => Salaries(value, accountNumber, date, description)
      case "credits" => Credits(value, accountNumber, date, description)
      case "purchase" => Purchase(value, accountNumber, date, description)
      case "withdrawal" => Withdrawal(value, accountNumber, date, description)
      case "debit" => Debit(value, accountNumber, date, description)
    }
  }


}

