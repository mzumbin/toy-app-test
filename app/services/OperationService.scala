package services

import java.time._
import javax.inject.{Inject, Singleton}

import com.google.inject.ImplementedBy
import db.OperationsDao
import model.Operation

import scala.annotation.tailrec
import scala.collection.{SeqView, immutable}

@ImplementedBy(classOf[OperationService])
trait BaseOperationService {

  /**
    * Insert a bank operation
    * @param operation the operation to be inserted
    */
  def addOperation(operation: Operation): Unit

  /**
    * Return all operations that belongs to the account
    * @param accountNumber the account number
    * @return an List containing all the operations
    */
  def getOperationsByAccount(accountNumber: String): List[Operation]

  /**
    * Get the total Balance for an account
    *
    * @param accountNumber
    * @param convert converter for the Operation class  to a numeric class
    * @param numeric
    * @tparam T
    * @return the account balance
    */
  def getBalance[T](accountNumber: String)(convert: Operation => T)(implicit numeric: Numeric[T]): T

  def clear(): Unit

  /**
    *
    * Get Balance for Period
    *
    * @param accountNumber
    * @param startTime start time
    * @param endTime   end time inclusive
    * @param convert   conversor for the Operation class to a numeric class
    * @param numeric
    * @tparam T
    * @return balance for a day with the operations made in that day filtered by start and end dates
    */
  def getPeriodsBalanceByDate[T](accountNumber: String, startTime: LocalDate, endTime: LocalDate)
                                (convert: Operation => T)(implicit numeric: Numeric[T]): List[(T, LocalDate, List[Operation])]

  /**
    * Process the debt periods
    *
    * @param accountNumber
    * @param convert conversor for the model to a numeric class
    * @param numeric
    * @tparam T numeric class
    * @return a List containing debt periods wih value, start and end date,
    *         the boolean value tells if account still is in debt
    */
  def getDebtPeriods[T](accountNumber: String)(convert: Operation => T)(implicit numeric: Numeric[T]): (List[(T, LocalDate, LocalDate)], Option[(T, LocalDate)])
}

@Singleton
class OperationService @Inject()(opDao: OperationsDao) extends BaseOperationService {
  private implicit val orddate = Ordering.by[java.time.LocalDate, java.time.chrono.ChronoLocalDate](identity)

  import Operation._

  override def addOperation(operation: Operation): Unit = opDao.insert(operation)

  override def getOperationsByAccount(accountNumber: String): List[Operation] = {
    opDao.getOperationsByAccount(accountNumber)
  }


  override def getBalance[T](accountNumber: String)(convert: Operation => T)(implicit numeric: Numeric[T]) = {
    val balance = opDao.getOperationsByAccount(accountNumber).map(convert(_)).sum
    balance
  }


  override def getPeriodsBalanceByDate[T](accountNumber: String, startDate: LocalDate, endDate: LocalDate)
                                         (convert: Operation => T)(implicit numeric: Numeric[T]): List[(T, LocalDate, List[Operation])] = {
    import scala.math.Ordering.Implicits._
    val groupBydate: Seq[(LocalDate, List[Operation])] = opDao.getOperationsByAccount(accountNumber).groupBy {
      op => stringInstantToLocaldate(op.date)
    }.toSeq.sortWith { case ((dateA, _), (dateB, _)) => dateA <= dateB}
    val sumOpsBydate: Seq[T] = groupBydate.map { case (_, dayOps) => dayOps.map(convert(_)).sum }
    import numeric._
    //accumulate  list of numerical balance in reverse order
    val (_, balanceByPeriodReverse): (T, List[T]) = sumOpsBydate.foldLeft((numeric.zero, List.empty[T])) {
      case ((totalsum, accPeriods), currentSum) => (totalsum + currentSum, (totalsum + currentSum) :: accPeriods)
    }
    for{
      (balance, (localDate, opList)) <-balanceByPeriodReverse.reverse zip groupBydate
      if startDate <= localDate && localDate <= endDate
    }yield (balance, localDate, opList)
  }

  override def clear(): Unit = opDao.clear()

  def getDebtPeriods[T](accountNumber: String)(convert: Operation => T)(implicit numeric: Numeric[T]): (List[(T, LocalDate, LocalDate)], Option[(T, LocalDate)]) = {

    import numeric._
    val balance = getPeriodsBalanceByDate(accountNumber, LocalDate.MIN, LocalDate.MAX)(convert)
    val last = balance.lastOption
    val lastIsNeg = last.map(l => l._1 < numeric.zero)
    val periods: Iterator[(T, LocalDate, LocalDate)] = for {
      balanceInsequence <- balance.sliding(2, 1) //  balanceA,balanceB,balanceC->  (balanceA,BalanceB),(BalanceB,BalanceC)
      (balanceA, dateA, _) = balanceInsequence(0)
      (_, dateB, _) = balanceInsequence(1)
      if balanceA < numeric.zero
    } yield (balanceA, dateA, dateB.minusDays(1))

    if (lastIsNeg.getOrElse(false))
      (periods.toList, Some((last.get._1, last.get._2)))
    else
      (periods.toList, None)
  }
}
