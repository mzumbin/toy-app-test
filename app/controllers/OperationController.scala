package controllers

import java.time.LocalDate
import javax.inject.{Inject, Singleton}

import model.Operation
import model.Operation.Purchase
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import services.{BaseOperationService, OperationService}

import scala.collection.immutable.List

@Singleton
class OperationController @Inject()(cc: ControllerComponents, operationService: BaseOperationService) extends AbstractController(cc) {

  import model.Operation._

  def addOperation = Action(parse.json) { request =>

    val op = Operation.readOperationJson(request.body)
    operationService.addOperation(op)
    Ok(Operation.operationToJson(op))
  }

  def balance(accountNumber: String) = Action { _ =>
    val balance = operationService.getBalance(accountNumber)(ValueBigDecimal.apply).toString
    Ok(Json.obj("balance"->balance,"accountNumber"-> accountNumber ))
  }


  def bankStatement(accountNumber: String, startDate: String, endDate: String) = Action { request =>
    val start = Operation.stringInstantToLocaldate(startDate)
    val end = Operation.stringInstantToLocaldate(endDate)
    val statement = operationService.getPeriodsBalanceByDate(accountNumber, start, end)(ValueBigDecimal.apply)
    val statementJs = statement.map { case (sum, day, ops) => {

      val opsJson = ops.map { op =>
        Json.obj("description" -> op.description,
          "value" -> op.value, "type" -> Operation.getypeString(op))
      }

      Json.obj(
        "balance" -> sum,
        "day" -> day.toString,
        "operations" -> opsJson

      )
    }
    }
    val result = Json.obj("accountNumber" -> accountNumber,"stament" -> statementJs)
    Ok(result)
  }

  def debtPeriods(accountNumber: String) = Action { request =>
    val (debtPeriods, debtlastBalance) = operationService.getDebtPeriods(accountNumber)(ValueBigDecimal.apply)

    val jsDebt = if (debtlastBalance.isDefined) {
      val endPeriod = Json.obj("value" -> debtlastBalance.get._1, "start" -> debtlastBalance.get._2)
      val js = debtPeriods.map { case (value,start, end) =>
        Json.obj(
        "value" -> value,
        "start" -> start,
        "end" -> end
        )}
      js ++ List(endPeriod)
    } else {
      debtPeriods.map { case (value, start, end) => Json.obj(
        "value" -> value,
        "start" -> start,
        "end" -> end
      )}
    }
    Ok(Json.obj("accountNumber"-> accountNumber,"debt_periods" -> jsDebt))
  }

}
