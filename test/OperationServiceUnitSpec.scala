import java.time.temporal.{ChronoUnit, TemporalUnit}
import java.time.{Instant, LocalDate}

import org.scalatest.Inspectors._
import db.{AccountDoNotExistException, OperationsDao, OperationsDaoMemory}
import model.Operation
import model.Operation.{Credits, Debit, Deposit, Purchase, Salaries, ValueBigDecimal, Withdrawal}
import org.scalatestplus.play.PlaySpec
import services.{BaseOperationService, OperationService}
import org.scalatest.Matchers._
import org.scalatest._

import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito._
class OperationServiceUnitSpec extends PlaySpec with org.scalatest.mockito.MockitoSugar{


  "Operation Service " should {

    val OpsWithPositiveEndBalance = List(
      Deposit (value ="771.43" , date= "2017-10-18T23:41:41.606Z", accountNumber="12"),
      Purchase( value ="800.00",date = "2017-10-18T23:43:41.606Z", accountNumber="12"),
      Purchase( value="10.00" ,date = "2017-10-22T23:43:41.606Z", accountNumber="12"),
      Deposit (value ="100.00" , date= "2017-10-25T23:43:41.606Z", accountNumber="12"))

    //(BigDecimal, LocalDate, List[Operation])=> Balance , date of operations, list of operations
    val bankStatementTest: Seq[(BigDecimal, LocalDate, List[Operation])] =
      List(
        (BigDecimal("1000.00"),LocalDate.parse("2017-10-15"),
          List( Deposit(value="1000.00",accountNumber="2",    "2017-10-15T19:08:25.159Z"))),
        (BigDecimal("951.43"),LocalDate.parse("2017-10-16"),
          List( Purchase(value="3.34", accountNumber="2", "2017-10-16T19:05:25.159Z",Some("Purchase on Amazon")),
            Purchase(value="45.23", accountNumber="2", "2017-10-16T20:05:28.159Z",Some("Purchase on Uber")))),
        (BigDecimal("771.43"),LocalDate.parse("2017-10-17"),
          List(Withdrawal(value="180.00",accountNumber="2","2017-10-17T19:05:25.159Z")))
      )



    "get balance and  get should return same list" in {
      val opDao = mock[OperationsDao]
      when(opDao.getOperationsByAccount("12")).thenReturn(OpsWithPositiveEndBalance)
      val osService = new OperationService(opDao)

      val balance = osService.getBalance(accountNumber="12")(ValueBigDecimal.apply)
      balance should equal (BigDecimal("61.43"))

    }




    "bank statement should return a valid stament" in {
      val opDao = mock[OperationsDao]
      val operationsInStatement = bankStatementTest.flatMap{case (_,_,operations)=>operations} //ops from stament
      // shuffle  the operations in the statement test
      when(opDao.getOperationsByAccount("2")).thenReturn( scala.util.Random.shuffle(operationsInStatement).toList)
      val osService = new OperationService(opDao)
      //range date includes all operations
      val result = osService.getPeriodsBalanceByDate("2", startDate=LocalDate.parse("2017-10-15"), endDate=LocalDate.parse("2017-10-17"))(ValueBigDecimal.apply)

      result.size should equal( bankStatementTest.size)
      val zipCompare = result zip bankStatementTest
      forAll((zipCompare))   { case ((_,_,opsResult),(_,_,opsTest)) => opsResult  should contain theSameElementsAs opsTest }
      forAll((zipCompare))   { case ((balance,_,_),(balanceTest,_,_)) => balance should equal (balanceTest)}
      forAll((zipCompare))   { case ((_,date,_),(_,dateTest,_)) => date should equal (dateTest)}

    }

    "bank stament should filtered by date correctly" in {

      val opDao = mock[OperationsDao]
      val operations = bankStatementTest.flatMap{case (_,_,operations)=>operations}
      // shuffle  the operations in the statement test
      when(opDao.getOperationsByAccount("2")).thenReturn( scala.util.Random.shuffle(operations).toList)
      val osService = new OperationService(opDao)
      //only stament in this day "2017-10-16"
      val fileredBankStament = bankStatementTest.filter({case (_,date,_)=>date.isEqual(LocalDate.parse("2017-10-16"))})
      val result = osService.getPeriodsBalanceByDate("2", LocalDate.parse("2017-10-16"), LocalDate.parse("2017-10-16"))(ValueBigDecimal.apply)

      result.size should equal( fileredBankStament.size)
      val zipCompare = result zip fileredBankStament
      forAll((zipCompare))   { case ((_,_,opsResult),(_,_,opsTest)) => opsResult  should contain theSameElementsAs opsTest }
      forAll((zipCompare))   { case ((balance,_,_),(balanceTest,_,_)) => balance should equal (balanceTest)}
      forAll((zipCompare))   { case ((_,date,_),(_,dateTest,_)) => date should equal (dateTest)}

    }

    " test negative periods of balance with a positive end balance" in {

      val opDao = mock[OperationsDao]
      when(opDao.getOperationsByAccount("2")).thenReturn( scala.util.Random.shuffle(OpsWithPositiveEndBalance))
      val osService = new OperationService(opDao)
      val (debtResult,lasInDebt) = osService.getDebtPeriods("2")(ValueBigDecimal.apply)

      val periodsOfDebt = List(
        (-28.57, LocalDate.parse("2017-10-18"),LocalDate.parse("2017-10-21")),
        (-38.57, LocalDate.parse("2017-10-22"),LocalDate.parse("2017-10-24")))

      debtResult should contain theSameElementsAs periodsOfDebt
      lasInDebt should equal(None)

    }

    " test negative periods of balance with a negative end balance  " in {
      val opsWithNegativeEndBalance = List(
        Deposit( value ="1000.00" , date = "2017-10-15T23:43:41.606Z",accountNumber="12"),
        Purchase(value= "3.34",     date = "2017-10-16T23:43:41.606Z",accountNumber="12",description=Some("purchase on amazon"))  ,
        Purchase(value="45.23",     date = "2017-10-16T23:43:42.606Z",accountNumber="12",description=Some("purchase on ubber")),
        Withdrawal(value="180.00" , date = "2017-10-17T23:43:42.606Z",accountNumber="12"),
        Purchase( value ="800.00",  date = "2017-10-18T23:43:41.606Z", accountNumber="12"),
        Purchase( value="10.00" ,   date = "2017-10-22T23:43:41.606Z", accountNumber="12"),
        Deposit ( value ="100.00" , date = "2017-10-25T23:43:41.606Z", accountNumber="12"),
        Purchase( value="500.23" ,  date = "2017-10-25T23:43:41.606Z", accountNumber="12"))
      val opDao = mock[OperationsDao]
      //shuffle the operations
      when(opDao.getOperationsByAccount("2")).thenReturn( scala.util.Random.shuffle(opsWithNegativeEndBalance))
      val osService = new OperationService(opDao)
      val  (datesPeriodsWithNegBalance,endWithNegative) = osService.getDebtPeriods(accountNumber="2")(ValueBigDecimal.apply)

      datesPeriodsWithNegBalance should contain theSameElementsAs
        List( (-28.57,LocalDate.parse("2017-10-18"),LocalDate.parse("2017-10-21")),
              (-38.57,LocalDate.parse("2017-10-22"),LocalDate.parse("2017-10-24")))

      endWithNegative should  equal( Some(-438.80,LocalDate.parse("2017-10-25")))

    }

  }


}
