import java.time.Instant

import db.AccountDoNotExistException
import model.Operation.{Purchase, ValueBigDecimal}
import org.scalatestplus.play.PlaySpec
import play.api.Mode
import play.api.inject.guice.GuiceApplicationBuilder
import services.BaseOperationService
import play.api.test.Helpers._
import org.scalatest.Inspectors._
import org.scalatest._
import org.scalatest.Matchers._
class DbFunctionalTest extends PlaySpec{


  " operation with db  " should {

    val appBuilder = new GuiceApplicationBuilder()
      .in(Mode.Test)
    val injector = appBuilder.injector()

    val os = injector.instanceOf[BaseOperationService]

    "persist and then get, should return same list with concurrent access" in {
      os.clear()
      val operations = (1 to 1000000).map(_ => Purchase("12", "21", Instant.now.toString))

      os.addOperation(operations.head)
      // parallel foreach
      operations.tail.par.foreach { op =>
        os.getOperationsByAccount(op.accountNumber)
        os.getOperationsByAccount(op.accountNumber)
        os.addOperation(op)
      }

      os.getOperationsByAccount("21") should contain theSameElementsAs (operations)

    }


    "get balance should throw an exception if a account dos not exist" in {

      an [AccountDoNotExistException] should be thrownBy os.getBalance[BigDecimal]("ads")(ValueBigDecimal.apply)
    }

  }



}
