

import javax.inject.Inject

import model.Operation
import model.Operation.{Deposit, Purchase, Withdrawal}
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.http.Status
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.Result
import play.api.test.{FakeHeaders, FakeRequest, Helpers}
import play.api.test.Helpers._
import play.test.WithApplication

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

/**
  * Functional tests start a Play application internally, available
  * as `app`.
  */

import scala.concurrent.ExecutionContext.Implicits.global

class IntegrationSpec extends PlaySpec with GuiceOneAppPerSuite {

  "operation controller " should {
    //account 17
    val operations = List(
      Deposit(value = "771.43",  date = "2017-10-18T23:41:41.606Z", accountNumber = "17", description = Some("deposito")),
      Purchase(value = "800.00", date = "2017-10-18T23:43:41.606Z", accountNumber = "17", description = Some("compra ipiranga")),
      Purchase(value = "10.00",  date = "2017-10-22T23:43:41.606Z", accountNumber = "17", description = Some("compra ipiranga2")),
      Deposit(value = "100.00",  date = "2017-10-25T23:43:41.606Z", accountNumber = "17", description = Some("deposito ipiranga")))
      //insert sequentially to test json output without

      //non blocking
      operations.foldLeft(Future.successful(Unit)){(future,operation)=>
        future.flatMap{ _ => route(app, FakeRequest(POST, "/operation").withJsonBody(Operation.operationToJson(operation))).get.map(_=>Unit)}}



    val opsWithNegativeEndBalance = List(
      Deposit( value ="1000.00" , date = "2017-10-15T23:43:41.606Z",accountNumber="18"),
      Purchase(value= "3.34",     date = "2017-10-16T23:43:41.606Z",accountNumber="18",description=Some("purchase on amazon"))  ,
      Purchase(value="45.23",     date = "2017-10-16T23:43:42.606Z",accountNumber="18",description=Some("purchase on ubber")),
      Withdrawal(value="180.00" , date = "2017-10-17T23:43:42.606Z",accountNumber="18"),
      Purchase( value ="800.00",  date = "2017-10-18T23:43:41.606Z", accountNumber="18"),
      Purchase( value="10.00" ,   date = "2017-10-22T23:43:41.606Z", accountNumber="18"),
      Deposit ( value ="100.00" , date = "2017-10-25T23:43:41.606Z", accountNumber="18"),
      Purchase( value="500.23" ,  date = "2017-10-25T23:43:41.606Z", accountNumber="18"))
    //insert sequentially to test json output

    // blocking
    opsWithNegativeEndBalance.map(op =>Await.result( route(app, FakeRequest(POST, "/operation").withJsonBody(Operation.operationToJson(op))).get,Duration.Inf))


    "return ok adding an operation" in {
      val op = Purchase(value = "771.43", date = "2017-10-18T23:41:41.606Z", accountNumber = "12", description = Some("purchase amazon"))
      val jsString = Operation.operationToJson(op)
      val resquest = FakeRequest(POST, "/operation").withJsonBody(jsString)

      val result: Future[Result] = route(app, resquest).get
      status(result) mustBe Status.OK
    }


    "should return a valid balance" in {
      //account 17
      val result = route(app, FakeRequest(GET, "/balance/17")).get
      status(result) mustBe Status.OK
      val responseJson = Json.parse( """{"balance":"61.43","accountNumber":"17"}""")
      contentAsJson(result) mustEqual (responseJson)
    }

    "should return a valid  bank stament" in {

      //account 17
      val result = route(app, FakeRequest(GET, "/statement/17/2017-10-18T23:41:41.606Z/2017-10-25T23:43:41.606Z")).get
      val responseJson = Json.parse("""{
                                      |	"accountNumber": "17",
                                      |	"stament": [{
                                      |		"balance": -28.57,
                                      |		"day": "2017-10-18",
                                      |		"operations": [{
                                      |			"description": "compra ipiranga",
                                      |			"value": "800.00",
                                      |			"type": "purchase"
                                      |		}, {
                                      |			"description": "deposito",
                                      |			"value": "771.43",
                                      |			"type": "deposit"
                                      |		}]
                                      |	}, {
                                      |		"balance": -38.57,
                                      |		"day": "2017-10-22",
                                      |		"operations": [{
                                      |			"description": "compra ipiranga2",
                                      |			"value": "10.00",
                                      |			"type": "purchase"
                                      |		}]
                                      |	}, {
                                      |		"balance": 61.43,
                                      |		"day": "2017-10-25",
                                      |		"operations": [{
                                      |			"description": "deposito ipiranga",
                                      |			"value": "100.00",
                                      |			"type": "deposit"
                                      |		}]
                                      |	}]
                                      |}""".stripMargin
      )
      status(result) mustBe Status.OK
      contentAsJson(result) mustEqual (responseJson)



    }

    "should return a valid debt period" in {
      //account 17
      val result = route(app, FakeRequest(GET, "/debtperiod/17")).get
      import scala.concurrent.ExecutionContext.Implicits.global._
      val r = contentAsJson(result)
      status(result) mustBe Status.OK
      val jsonResponse = Json.parse("""{
                                      |	"accountNumber": "17",
                                      |	"debt_periods": [{
                                      |		"value": -28.57,
                                      |		"start": "2017-10-18",
                                      |		"end": "2017-10-21"
                                      |	}, {
                                      |		"value": -38.57,
                                      |		"start": "2017-10-22",
                                      |		"end": "2017-10-24"
                                      |	}]
                                      |}""".stripMargin)

      contentAsJson(result) mustEqual (jsonResponse)

    }


    "should return a valid debt period with no end(still in debit)" in {
      //account 18 end with negative balance
      val result = route(app, FakeRequest(GET, "/debtperiod/18")).get
      import scala.concurrent.ExecutionContext.Implicits.global._
      val r = contentAsJson(result)
      status(result) mustBe Status.OK
      val jsonResponse = Json.parse("""{
                                      |	"accountNumber": "18",
                                      |	"debt_periods": [{
                                      |		"value": -28.57,
                                      |		"start": "2017-10-18",
                                      |		"end": "2017-10-21"
                                      |	}, {
                                      |		"value": -38.57,
                                      |		"start": "2017-10-22",
                                      |		"end": "2017-10-24"
                                      |	}, {
                                      |		"value": -438.8,
                                      |		"start": "2017-10-25"
                                      |	}]
                                      |}""".stripMargin)

      contentAsJson(result) mustEqual (jsonResponse)

    }


  }
}
