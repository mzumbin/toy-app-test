
# Toy App Test
A checking account from a bank allows for putting (deposits, salaries, credits)
or taking (purchases, withdrawals, debits) money at any given time.

You can  check what is your current balance or your account statement,
containing all operations that happened between two dates, along with the
account's daily balance.

 you can also  checks the periods which the account's balance
  was negative, i.e, periods when the bank can charge interest on that account.
  
see IntegrationSpec.scala
and OperationSpec.scala
## Running

Run this using [sbt](http://www.scala-sbt.org/).  

```
sbt run
```
Test application using
```
sbt test
```




## Controllers

OperationController.scala:

  expose OperationService.scala to HTTP requests.



## Model

 Operation.scala:

   contain all the models used in application



## Services

 OperationService.scala: services only use pure functions,
   all the logic are here.
  
 Operation Service can use any numeric, the tests uses the big decimal class, but this service could use  double  representation to speed up the calculations

  

##Tests
 
 OperationServiceUnitSpec.scala:
   unit  test for the balance computation
  
 IntegrationSpec:
   integration test
   
DbFunctionalTest:
test the "db"   
   